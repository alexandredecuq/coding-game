import java.util.*;

class Pair<X, Y> {
    public final X first;
    public final Y second;
    public Pair(X x, Y y) {
        this.first = x;
        this.second = y;
    }
}

class Tree implements Comparable<Tree>{
    Cell c; // location of this tree
    int size; // size of this tree: 0-3
    boolean isDormant; // 1 if this tree is dormant
    Tree(int i, int s, boolean b) {
        if(!Game.board.containsKey(i))
            System.err.println("Game.board doesn't have key " + i);
        c = Game.board.get(i);
        size = s;
        isDormant = b;
    }
    int position() {
        return c.position;
    }
    @Override
    public int compareTo(Tree t){
        return Integer.compare(this.c.richness, t.c.richness);
    }
}

class Gamer {
    HashMap<Integer, Tree> trees;
    int sunPoints;
    int score;
}

class Cell {
    int position; // 0 is the center cell, the next cells spiral outwards
    int richness; // 0 if the cell is unusable, 1-3 for usable cells
    List<Integer> adjCells;
    int score; // computed when evaluated
    Cell(int i, int r, List<Integer> n, int s) {
        position = i;
        richness = r;
        adjCells = n;
        score = s;
    }
}

class SeedTracker {
    boolean alreadySeedThisDay;
    int dayTracker;
    void reset() {
        if (Game.day != dayTracker)
            alreadySeedThisDay = false;
    }
    void update() {
        alreadySeedThisDay = true;
        dayTracker = Game.day;
    }
}

class Game {
    static Gamer me = new Gamer();
    static Gamer enemy = new Gamer();
    static HashMap<Integer, Cell> board;
    static HashMap<Integer, List<Integer>> hexagonalBoard;
    static int day;
    static int nutrients;
    static SeedTracker seedTracker = new SeedTracker();

    public static boolean cellIsAvailable(int position) {
        boolean res = !Game.me.trees.containsKey(position) && !Game.enemy.trees.containsKey(position);
        //System.err.println("cell " + position + " availability=" + res);
        return !Game.me.trees.containsKey(position) && !Game.enemy.trees.containsKey(position);
    }

    public static int numberOfMyTrees(int size) {
        int cnt = 0;
        for(Tree t : Game.me.trees.values())
            if(t.size == size) cnt++;
        return cnt;
    }

    public static int remainingDays() {
        return 24 - day;
    }


    public static List<Tree> myBiggestTree() {
        List<Tree> l = new LinkedList<>();
        for (Tree t : Game.me.trees.values()) {
            if(t.size == 3)
                l.add(t);
        }
        return l;
    }

    public static Tree getTreeFromPosition(int pos) {
        if (Game.me.trees.containsKey(pos)) {
            return Game.me.trees.get(pos);
        } else return Game.enemy.trees.getOrDefault(pos, null);
    }
}

class Player {

    public static final int INIT_EVAL_SEED_SCORE = -7;

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        updateBoardInfo(in);

        // game loop
        while (true) {
            updateTurnInfo(in);
            System.err.println("remaining days = " + Game.remainingDays());

            if(Game.day == 0) {
                System.out.println("WAIT Greetings!");
            } else if(isEndGame()) {
                int pos = findRichestSpotForGrowthOrCut(3);
                if(pos != -1) System.out.println("COMPLETE " + pos);
                else System.out.println("WAIT I am done.");
            } else {
                basicStrategy();
            }
        }
    }

    private static boolean isEndGame() {
        return (Game.remainingDays() <= Game.numberOfMyTrees(3)) || Game.remainingDays() == 1;
    }

    private static void basicStrategy() {
        if (isAnnoyingShadow()) return;

        if(Game.me.sunPoints >= computeMyGrowCost(2) && Game.numberOfMyTrees(2) > 0) {
            System.err.println("findRichestSpotForGrowth-2");
            int pos = findRichestSpotForGrowthOrCut(2);
            if(pos!=-1) {
                System.out.println("GROW " + pos);
                return;
            }
        } if (Game.me.sunPoints >= computeMyGrowCost(1) && Game.numberOfMyTrees(2) < 1
                && Game.remainingDays() >= 2) {
            System.err.println("findRichestSpotForGrowth-1");
            int pos = findRichestSpotForGrowthOrCut(1);
            if(pos!=-1) {
                System.out.println("GROW " + pos);
                return;
            }
        } if (Game.me.sunPoints >= computeMyGrowCost(0) && Game.numberOfMyTrees(1) < 1
                && Game.remainingDays() >= 3) {
            System.err.println("findRichestSpotForGrowth-0");
            int pos = findRichestSpotForGrowthOrCut(0);
            if(pos!=-1) {
                System.out.println("GROW " + pos);
                return;
            }
        }
        tryToSeed();
    }

    private static boolean isAnnoyingShadow() {
        List<Tree> l = Game.myBiggestTree();
        for(Tree t : l) {
            if (isAnnoyingShadowForTree(t)) {
                System.out.println("COMPLETE " + t.position());
                return true;
            }
        }
        return false;
    }

    private static boolean isAnnoyingShadowForTree(Tree t) {
        int shadowQty = getShadowQty(3, t.position(), t.size);
        int shadowQtyNextDay = getShadowQty(1, t.position(), t.size);
        System.err.println("shadowQty for " + t.position() + " = " + shadowQty);
        return shadowQty >= 2 && shadowQtyNextDay == 0;
    }

    private static int getShadowQty(int days, int origin, int size) {
        if(origin==-1)
            return -1000; // out of board
        int shadowQty = 0;
        for(int i = 1; i<= days; i++) {
            int shadowDirection = (Game.day+i) % 6;
            int exploreDirection = (shadowDirection + 3) % 6;
            int pos = origin;
            for(int j=0;j<3;j++) {
                Integer neigh = Game.hexagonalBoard.get(pos).get(exploreDirection);
                pos = neigh;
                if(neigh == -1)
                    break;
                Tree n = Game.getTreeFromPosition(neigh);
                if (n == null ) {
                    continue; //no tree
                }
                if (n.size >= size) {
                    shadowQty++;
                    break;
                }
            }
        }
        return shadowQty;
    }

    private static void tryToSeed() {
        if(!Game.seedTracker.alreadySeedThisDay) {
            Pair seedSpot = findBestSeedSpot();
            if(seedSpot != null) {
                System.out.println("SEED " + seedSpot.first + " " + seedSpot.second);
                Game.seedTracker.update();
            }
            else
                System.out.println("WAIT");
        } else {
            System.out.println("WAIT");
        }
    }

    private static int computeMyGrowCost(int size) {
        if(size == 0)
            return 1 + Game.numberOfMyTrees(1);
        else if (size == 1)
            return 3 + Game.numberOfMyTrees(2);
        else
            return 7 + Game.numberOfMyTrees(3);
    }

    private static int findRichestSpotForGrowthOrCut(int targetSize) {
        PriorityQueue<Tree> q = new PriorityQueue<>(Collections.reverseOrder());
        for(Tree t : Game.me.trees.values()) {
            if(t.size == targetSize && !t.isDormant)
                q.add(t);
        }
        if(q.isEmpty())
            return -1;
        else
            return q.poll().position();
    }

    private static Pair findBestSeedSpot() {
        int bestScore = INIT_EVAL_SEED_SCORE;
        Pair bestPosition = null;
        for(Tree t : Game.me.trees.values()) {
            System.err.println("> explore from " + t.position());
            if(t.size <= 1 || t.isDormant)
                continue; //exclude seeds and small trees
            Cell c = dfs(t.position(), t.size);
            System.err.println("bestPosition found = " + c.score + " at " + c.position);
            if(c.score >= bestScore && c.position != -1)
                bestPosition = new Pair(t.position(), c.position);
        }
        return bestPosition;
    }

    private static Cell dfs(int pos, int remainingDeep) {
        //TODO: implemented visited
        int bestScore = INIT_EVAL_SEED_SCORE;
        Cell curr = Game.board.get(pos);
        if(remainingDeep == 0) // Game.cellIsAvailable(curr.position) ||
            return curr;
        Cell bestPosition = new Cell(-1,0,new LinkedList<>(), INIT_EVAL_SEED_SCORE);
        for(int i : curr.adjCells) {
            Cell c = dfs(i, remainingDeep-1);
            int shadowWQty = getShadowQty(6, c.position, 1);
            int score = evaluateSeedSpot(shadowWQty, c.richness);
            c.score = score;
            if(score >= bestScore && Game.cellIsAvailable(c.position)) {
                bestPosition = c;
                bestScore = score;
            }
        }
        return bestPosition;
    }

    private static int evaluateSeedSpot(int shadowWQty, int richness) {
        return richness * 2 - shadowWQty;
    }

    private static void updateTurnInfo(Scanner in) {
        Game.day = in.nextInt(); // the game lasts 24 days: 0-23
        Game.nutrients = in.nextInt(); // the base score you gain from the next COMPLETE action
        Game.me.sunPoints = in.nextInt(); // your sun points
        Game.me.score = in.nextInt(); // your current score
        Game.enemy.sunPoints = in.nextInt(); // opponent's sun points
        Game.enemy.score = in.nextInt(); // opponent's score
        Game.seedTracker.reset();
        boolean oppIsWaiting = in.nextInt() != 0; // whether your opponent is asleep until the next day
        int numberOfTrees = in.nextInt(); // the current amount of trees
        Game.me.trees =  new HashMap<>();
        Game.enemy.trees  = new HashMap<>();
        for (int i = 0; i < numberOfTrees; i++) {
            int cellIndex = in.nextInt();
            int size = in.nextInt();
            boolean isMine = in.nextInt() != 0;
            boolean isDormant = in.nextInt() != 0;
            Tree t = new Tree(cellIndex,size,isDormant);
            if (isMine)
                Game.me.trees.put(cellIndex, t);
            else
                Game.enemy.trees.put(cellIndex, t);
        }
        int numberOfPossibleMoves = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        for (int i = 0; i < numberOfPossibleMoves; i++) {
            String possibleMove = in.nextLine(); //ignore
        }
    }

    private static void updateBoardInfo(Scanner in) {
        Game.board = new HashMap<>();
        Game.hexagonalBoard = new HashMap<>();
        int numberOfCells = in.nextInt(); // 37
        for (int i = 0; i < numberOfCells; i++) {
            int index = in.nextInt();
            int richness = in.nextInt();
            List<Integer> adjCells = new LinkedList<>();
            List<Integer> hexagonalAdjCells = new LinkedList<>();
            for(int j=0;j<6;j++) {
                int adj= in.nextInt();
                if(adj!=-1) adjCells.add(adj);
                hexagonalAdjCells.add(adj);
            }
            Game.board.put(index, new Cell(index,richness,adjCells,INIT_EVAL_SEED_SCORE));
            Game.hexagonalBoard.put(index, hexagonalAdjCells);
        }
    }
}
